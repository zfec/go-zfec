// Package zfec is the Go API for the zfec fast forward error correction library.
// More information about zfec can be found here: https://pypi.python.org/pypi/zfec
package zfec

/* zfec.go
 *
 * Copyright (C) 2013 Simon Liu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

//#cgo CFLAGS: -std=gnu99
//#include "fec.h"
import "C"

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"runtime"
	"unsafe"
)

const (
	kChunkSize    = 4096
	VersionString = "1.4.24" // from zfec tarball, found in _version.py
)

// Zfec is a struct wrapping up C.fec_t.
// K is the number of packets required for reconstruction.
// M is the number of packets generated.
// Ptr is a C pointer to fec_t returned by fec_new().
type Zfec struct {
	K   int
	M   int
	Ptr unsafe.Pointer
}

// div_ceil returns the smallest integer k such that k*d >= n.
// The equivalent function in Python: return (n/d) + (n%d != 0)
func div_ceil(n int, d int) int {
	return int(math.Ceil(float64(n) / float64(d)))
}

// log_ceil returns the smallest integer k such that b^k >= n.
// log_ceil(n, 2) is the number of bits needed to store any of n values,
// e.g. the number of bits needed to store any of 128 possible values is 7.
func log_ceil(n int, b int) int {
	p := 1
	k := 0
	for p < n {
		p *= b
		k++
	}
	return k
}

// PadSize returns the smallest number that has to be added to n to equal a multiple of k.
func PadSize(n int, k int) int {
	if n%k > 0 {
		return k - n%k
	} else {
		return 0
	}
}

// Init creates a valid zfec struct which can be used for encoding or decoding.
// The caller should invoke Dealloc() on the Zfec struct to release underlying resources.
func Init(k int, m int) (*Zfec, error) {
	if k < 1 {
		return nil, fmt.Errorf("Precondition violation: first argument is required to be greater than or equal to 1, but it was %d", k)
	}
	if m < 1 {
		return nil, fmt.Errorf("Precondition violation: second argument is required to be greater than or equal to 1, but it was %d", m)
	}
	if m > 256 {
		return nil, fmt.Errorf("Precondition violation: second argument is required to be greater than or equal to 1, but it was %d", m)
	}
	if k > m {
		return nil, fmt.Errorf("Precondition violation: first argument is required to be less than or equal to the second argument, but they were %d and %d respectively", k, m)
	}

	var f Zfec
	f.K = k
	f.M = m
	cp := C.fec_new(C.ushort(f.K), C.ushort(f.M))
	if cp == nil {
		return nil, fmt.Errorf("fec_new(%d,%d) returned nil", k, m)
	}
	f.Ptr = unsafe.Pointer(cp)
	return &f, nil
}

// Dealloc frees the fec_t struct which was allocated by Zfec via Init()
func (f Zfec) Dealloc() {
	if f.Ptr != nil {
		C.fec_free((*C.fec_t)(f.Ptr))
		f.Ptr = nil
	}
}

// EncodeToFiles will encode a file and write share blocks to an output directory.
// The caller is responsible for making sure the output directory is valid.
// The encoded shares will will add an optional prefix and suffix to the base filename.
// A suffix is always preceded by a period "." and the default suffix is "fec".
// The force boolean is true if you want to overwrite existing data.
// If the verbose boolean is true, print out some information.
// The default value of cpus is maximum number of cores available in system.
// A cpus value of 1 effectively turns off parallel encoding.
// If the base is empty, the source filename will be used.
// Returns an array of paths to the shares.
func (f Zfec) EncodeToFiles(path string, outdir string, base string, prefix string, suffix string, force bool, verbose bool, cpus int) (filenames []string, err error) {

	// set defaults
	if base == "" {
		base = filepath.Base(path)
	}
	if suffix == "" {
		suffix = "fec"
	}

	// is outdir valid?
	dirinfo, err := os.Stat(outdir)
	if err != nil {
		return nil, err
	}
	if !dirinfo.IsDir() {
		return nil, err
	}

	fi, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	// close fi on exit and check for its returned error
	defer func() {
		fi.Close()
	}()

	// make a read buffer
	r := bufio.NewReader(fi)
	finfo, err := fi.Stat()
	fsize := finfo.Size()

	m := f.M
	k := f.K
	writers := make([]*bufio.Writer, m)

	files := make([]*os.File, m)

	filenames = make([]string, m)

	padlen := PadSize(int(fsize), k)

	var newname string
	for i := 0; i < m; i++ {

		if m <= 99 {
			newname = fmt.Sprintf("%s%s.%02d_%02d.%s", prefix, base, i, m, suffix)
		} else {
			newname = fmt.Sprintf("%s%s.%03d_%03d.%s", prefix, base, i, m, suffix)
		}
		newpath := filepath.Join(outdir, newname)
		flags := os.O_RDWR | os.O_CREATE
		if !force {
			flags |= os.O_EXCL
		}
		fi, err := os.OpenFile(newpath, flags, 0666)
		if err != nil {
			return nil, err
		}

		if verbose {
			fmt.Printf("Creating share file '%s'...\n", newpath)
		}

		filenames[i] = newpath
		defer func() {
			fi.Close()
		}()
		header, err := f.CreateZfecFileHeaderForShare(i, padlen)
		if err != nil {
			return nil, err
		}

		writers[i] = bufio.NewWriter(fi)
		files[i] = fi
		writers[i].Write(header)
		writers[i].Flush()
	}

	// Callback
	var sumlen int64
	cb := func(blocks [][]byte, length int) {

		if verbose {
			oldsumlen := sumlen
			sumlen += int64(length)
			a := int(float64(oldsumlen) / float64(fsize) * 10.0)
			b := int(float64(sumlen) / float64(fsize) * 10.0)
			if a != b {
				fmt.Printf("%d%%... ", b*10)
				if b == 10 {
					fmt.Printf("\n")
				}
			}
		}

		//fmt.Printf("%d, %d, length = %d\n", len(blocks), len(blocks[0]), length)
		for i, _ := range blocks {
			buf := blocks[i]
			writers[i].Write(buf)
			writers[i].Flush()
		}
	}

	count, err := f.EncodeReaderParallel(r, cb, cpus)
	if err != nil {
		return nil, err
	}
	if count != uint64(fsize) {
		return nil, fmt.Errorf("Source file has %d bytes, but %d bytes were read\n", fsize, count)
	}

	if verbose {
		fmt.Println("Done!")
	}

	return filenames, err
}

// DecodeToFile will decode shares and write output to target path.
func DecodeToFile(target string, srcpaths []string, force bool, verbose bool) error {
	flags := os.O_RDWR | os.O_CREATE
	if !force {
		flags |= os.O_EXCL
	}
	decodedFile, err := os.OpenFile(target, flags, 0666)
	//decodedFile, err := os.Create(target)
	if err != nil {
		return err
	}
	defer func() {
		decodedFile.Close()
	}()

	n := len(srcpaths)

	files := make([]*os.File, n)
	for i, p := range srcpaths {
		fi, err := os.Open(p)
		if err != nil {
			return err
		}
		files[i] = fi
	}

	w := bufio.NewWriter(decodedFile)
	defer func() {
		w.Flush()
	}()
	err = DecodeToWriter(w, files, verbose)
	if err != nil {
		return err
	}

	return nil
}

// EncodeReaderCallbackFunc is a type representing a functor which does something with encoded data.
// The functor has two input parameters, the first a slice of pointers to Buffers containin encoded data.
// The second is the length of the source data prior to being encoded.
type EncodeReaderCallbackFunc func([][]byte, int)

// EncodeReader encodes input data from a Reader, and then invokes a callback with the encoded data.
// Data is slurped up in chunks, with a size of (k * 4096) bytes, until EOF.
// Returns a count of the amount of data read and any error to the caller.
func (f Zfec) EncodeReader(r io.Reader, callback EncodeReaderCallbackFunc) (count uint64, err error) {
	p := make([]byte, f.K*kChunkSize)
	for {
		n, err := r.Read(p)
		if n == 0 && err == io.EOF {
			break
		} else if err != nil {
			return 0, err
		} else if n < f.K*kChunkSize {
		}

		count = count + uint64(n)
		blocks, _, err := f.EncodeBytes(p[0:n]) // [0:n] for incoming data length, not capacity
		if err != nil {
			return 0, err
		}

		callback(blocks, n)
	}
	return count, nil
}

// EncodeReaderParallel encodes input data from a Reader, and then invokes a callback with the encoded data.
// Cpus is number of processors to use, 0 or invalid number results in default, which is maximum number of cores available to system.
// Data is slurped up in chunks, with a size of (k * 4096) bytes, until EOF.
// Returns a count of the amount of data read and any error to the caller.
func (f Zfec) EncodeReaderParallel(r io.Reader, callback EncodeReaderCallbackFunc, cpus int) (count uint64, err error) {
	if cpus == 1 {
		return f.EncodeReader(r, callback)
	}

	cpusMax := runtime.NumCPU()
	if cpus < 1 || cpus > cpusMax {
		cpus = cpusMax
	}
	cpus = runtime.GOMAXPROCS(cpus)

	jobs := make(chan encodeJob, cpus*100)
	results := make(chan encodeResult, cpus*100)
	// NOTE: WaitGroup does not work as expected, so for now we use done channel
	//	var wg sync.WaitGroup
	done := make(chan int, cpus)
	errors := make(chan error, 1)
	abort := false
	var abortErr error

	// Goroutine sets abort flag if error received on errors channel
	go func() {
		for err := range errors {
			if !abort {
				abortErr = err
				abort = true
			}
		}
	}()

	// Goroutine to ensure result callbacks are executed in sequence of input data
	go f.resultWorker(results, callback, done)

	// Goroutine workers to process input data
	for i := 0; i < cpus; i++ {
		//	wg.Add(1)
		go f.worker(i, jobs, results, errors, done)
	}

	counter := 1

	p := make([]byte, f.K*kChunkSize)
	for {
		if abort {
			break
		}

		n, err := r.Read(p)
		if n == 0 && err == io.EOF {
			break
		} else if err != nil {
			return 0, err
		} else if n < f.K*kChunkSize {
		}

		count = count + uint64(n)

		inputData := make([]byte, n)
		copy(inputData, p[0:n])
		job := encodeJob{buf: inputData, id: counter}
		jobs <- job
		counter++
	}

	close(jobs)
	//	wg.Wait()
	for i := 0; i < cpus; i++ {
		<-done
	}
	close(results)
	<-done
	close(done)
	close(errors)
	return count, abortErr
}

type encodeJob struct {
	buf []byte
	id  int
}

type encodeResult struct {
	buf [][]byte
	id  int
}

func (f Zfec) worker(i int, jobs chan encodeJob, results chan encodeResult, errors chan error, done chan int) {
	//	defer wg.Done()
	var err error
	for j := range jobs {
		// Don't break out of range otherwise goroutine leaks
		// http://stackoverflow.com/questions/1720201/go-examples-and-idioms#comment4498244_1779183
		if err != nil {
			continue
		}
		blocks, _, err := f.EncodeBytes(j.buf)
		if err != nil {
			errors <- err
		} else {
			outputData := make([][]byte, len(blocks))
			for i, b := range blocks {
				tmp := make([]byte, len(b))
				copy(tmp, b)
				outputData[i] = tmp
			}
			r := encodeResult{buf: outputData, id: j.id}
			results <- r
		}
	}
	done <- 1
}

func (f Zfec) resultWorker(results chan encodeResult, callback EncodeReaderCallbackFunc, done chan int) {
	var m = map[int][][]byte{}
	counter := 1
	received := 0
	for r := range results {
		received++
		m[r.id] = r.buf
		if m[counter] != nil {
			callback(m[counter], len(m[counter]))
			m[counter] = nil
			counter++
		}
	}

	// Finish callbacks for all results
	for i := counter; i <= received; i++ {
		if m[i] != nil {
			callback(m[i], len(m[i]))
			m[i] = nil
		} else {
			// This shouldn't happen
			// fmt.Printf("map[%d] is nil\n", i)
		}
	}

	done <- 1
}

// CreateZfecFileHeaderForShare computes a file header compatible with the zfec python tool.
// Invokes BuildZfecFileHeader using the fec's k and m.  Returns a slice of bytes.
func (f Zfec) CreateZfecFileHeaderForShare(n int, padlen int) ([]byte, error) {
	return BuildZfecFileHeaderForShare(f.M, f.K, n, padlen)
}

// BuildZfecFileHeader computes a file header compatible with the zfec python tool.
// Share number is passed in argument n.  Returns a slice of bytes.
func BuildZfecFileHeaderForShare(m int, k int, n int, padlen int) ([]byte, error) {
	// First 8 bits + 1 = m
	// Next x bits (logceil of m) contains k-1 (so plus 1 to get real k)
	//    shbits = log_ceil(m, 2) # num bits needed to store all possible values of shnum
	//    padbits = log_ceil(k, 2) # num bits needed to store all possible values of pad
	kbits := log_ceil(m, 2)
	shbits := log_ceil(m, 2)
	padbits := log_ceil(k, 2)
	bitsused := 8 + kbits + shbits + padbits

	var x uint32 = 0
	x = x | uint32(m-1)
	x = x << uint32(kbits)
	x = x | uint32(k-1)
	x = x << uint32(padbits)
	x = x | uint32(padlen)
	x = x << uint32(shbits)
	x = x | uint32(n)
	x = x << (32 - uint(bitsused))

	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.BigEndian, x)
	if err != nil {
		return nil, err
	}
	b := buf.Bytes()

	if bitsused <= 8 {
		b = nil
	} else if bitsused <= 16 {
		b = b[0:2]
	} else if bitsused <= 24 {
		b = b[0:3]
	} else if bitsused <= 32 {
		b = b[0:4]
	} else {
		b = nil
	}

	return b, nil
}

// ParseZfecFileHeader will parse a Zfec header which precedes a primary or secondary block.
// Input zfec headers are usually from 2-4 bytes in length.
// Returns length of header in bytes, as well as m, k, padding length and share number of block.
func ParseZfecFileHeader(header []byte) (headerlen int, m int, k int, padlen int, share int, err error) {
	if len(header) == 0 || len(header) > 4 {
		return 0, 0, 0, 0, 0, fmt.Errorf("Length of header, %d, is not valid", len(header))
	}

	//Make sure there are 4 bytes to read
	if len(header) < 4 {
		tmpbuf := make([]byte, 4)
		copy(tmpbuf, header)
		header = tmpbuf
	}

	buf := bytes.NewBuffer(header)
	var x uint32
	err = binary.Read(buf, binary.BigEndian, &x)

	if err != nil {
		return 0, 0, 0, 0, 0, err
	}

	bitsused := 0

	// The first 8 bits always encode m
	tmp := (x >> 24) + 1
	m = int(tmp)
	bitsused += 8

	// num bits needed to store all possible values of k
	kbits := log_ceil(m, 2)
	tmp = ((x << uint(bitsused)) >> uint(32-kbits)) + 1
	k = int(tmp)
	bitsused += kbits

	// num bits needed to store all possible values of pad
	padbits := log_ceil(k, 2)
	tmp = ((x << uint(bitsused)) >> uint(32-padbits))
	padlen = int(tmp)
	bitsused += padbits

	// num bits needed to store all possible values of shnum
	shbits := log_ceil(m, 2)
	tmp = ((x << uint(bitsused)) >> uint(32-shbits))
	share = int(tmp)
	bitsused += shbits

	p := 0
	if bitsused <= 8 {
		p = 1
	} else if bitsused <= 16 {
		p = 2
	} else if bitsused <= 24 {
		p = 3
	} else if bitsused <= 32 {
		p = 4
	}

	return p, m, k, padlen, share, nil
}

// DecodeToWriter will decode from share files and output to a buffered writer.
func DecodeToWriter(w *bufio.Writer, files []*os.File, verbose bool) error {
	var m, k, padlen int
	var sharemap = map[int]bool{}
	var indices = []int{}
	buf := make([]byte, 4)
	for _, f := range files {
		n, err := f.ReadAt(buf, 0)
		if err != nil && err != io.EOF {
			return fmt.Errorf("File may be corrupted, cannot read zfec file header, only received %d bytes", n)
		}
		headerlen, mm, kk, pl, share, err := ParseZfecFileHeader(buf)
		//fmt.Printf("PARSED ZFEC FILE HEADER: %d, %d, %d, %d\n", mm, kk, pl, share)
		if m == 0 {
			m = mm
		}
		if k == 0 {
			k = kk
		}
		if padlen == 0 {
			padlen = pl
		}
		if sharemap[share] == true {
			return fmt.Errorf("Error, duplicate shares claiming to be share block number %d", share)
		}
		if m != mm {
			return fmt.Errorf("Error, a share claims m is %d, but another share claims it is %d", m, mm)
		}
		if k != kk {
			return fmt.Errorf("Error, a share claims k is %d, but another share claims it is %d", k, kk)
		}
		if padlen != pl {
			return fmt.Errorf("Error, a share claims pad length is %d, but another share claims it is %d", padlen, pl)
		}

		sharemap[share] = true
		m = mm
		k = kk
		padlen = pl
		indices = append(indices, share)

		// set file position for next read
		p := int64(headerlen)
		q, err := f.Seek(p, 0)
		if q != p || err != nil {
			return fmt.Errorf("Error, could not seek to position %d", p)
		}
	}

	if k > len(files) {
		return fmt.Errorf("Number of input files %d is insufficient, need at least k %d", len(files), k)
	}

	// If there are too many input files, we only need the first k
	if len(files) > k {
		files = files[0:k]
		indices = indices[0:k]
	}

	fec, err := Init(k, m)

	if err != nil {
		return fmt.Errorf("Invalid k and m")
	}

	defer func() {
		fec.Dealloc()
	}()

	//fmt.Printf("k = %d, m = %d, padlen = %d, indices = %v\n", k, m, padlen, indices)

	blocks := make([][]byte, k)
	readbuf := make([]byte, kChunkSize*k)
	var padding int
	var bytesread int64

	// for verbose mode, to count in units of 10MB
	var bytesWritten int64
	var tenMBCounter int

	// fec chunk size is file size less header len
	p, _ := files[0].Seek(0, os.SEEK_CUR)
	finfo, err := files[0].Stat()
	chunksize := finfo.Size() - p

	for true {
		eof := false
		n := 0
		// Read in encoded data
		// Todo: keep better check of file length to see if all the same etc.
		for i, f := range files {
			n, err = f.Read(readbuf[i*kChunkSize : (i+1)*kChunkSize])

			if n == 0 && err == io.EOF {
				eof = true
				break
			} else if err != nil {
				return fmt.Errorf("Error reading data to decode")
			}
			blocks[i] = readbuf[i*kChunkSize : (i*kChunkSize)+n]

			//fmt.Printf("%d : read %d bytes, stored at %p\n", i, n, blocks[i].Bytes() )
		}
		if eof {
			break
		}

		bytesread += int64(n)

		// Need padding on last read which is when the read is not full, or if fec chunk size is multiple of kChunkSize
		// https://tahoe-lafs.org/pipermail/tahoe-dev/2013-September/008702.html
		if kChunkSize == n && bytesread < chunksize {
			padding = 0
		} else {
			padding = padlen
		}

		decodedData, err := fec.Decode(blocks, indices, padding)

		if err != nil {
			return err
		}
		_, err = w.Write(decodedData)
		if err != nil {
			return err
		}

		if verbose {
			bytesWritten += int64(len(decodedData))
			a := int(bytesWritten / (10 * 1024 * 1024))
			if a != tenMBCounter {
				tenMBCounter = a
				fmt.Printf("%d MB... ", a*10)
			}
		}
	}
	w.Flush()

	if verbose {
		fmt.Println("\nDone!")
	}

	return nil
}

// EncodeFile is a convenience function to encode a file.
// Input is a path to the source file which will be completely loaded into memory.
// Returns primary and secondary blocks, in order, in slice of pointers to Buffers, as well as padding length and any error.
func (f Zfec) EncodeFile(filename string) (blocks [][]byte, padlen int, err error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, 0, err
	}
	blocks, padlen, err = f.EncodeBytes(b)
	if err != nil {
		return nil, 0, err
	}
	return blocks, padlen, nil
}

// EncodeBytes will encode a byte slice, returning m blocks, in order, as an array of references to Buffers.
// The returned padlen is the padding used for the final block m.
func (f Zfec) EncodeBytes(b []byte) (blocks [][]byte, padlen int, err error) {
	chunksize := div_ceil(len(b), f.K)
	padlen = PadSize(len(b), f.K)

	if padlen > 0 {
		padding := bytes.Repeat([]byte{0x00}, padlen)
		b = append(b, padding...)
	}

	primary := make([][]byte, f.K)
	for i := 0; i < f.K; i++ {
		primary[i] = b[i*chunksize : (i+1)*chunksize]
	}

	secondary, _, err := f.Encode(primary)
	if err != nil {
		return nil, 0, err
	}

	blocks = make([][]byte, f.M)
	for i := 0; i < f.M; i++ {
		if i < f.K {
			blocks[i] = primary[i]
		} else {
			blocks[i] = secondary[i-f.K]
		}
	}

	return blocks, padlen, nil
}

// Encode will encode k primary blocks and return m-k secondary blocks.
// As input, primary blocks must all be the same length.
// Secondary blocks are returned, in order, as an array of references to Buffer, with their share number returned in indices.
func (f Zfec) Encode(inblocks [][]byte) (blocks [][]byte, indices []int, err error) {
	num_inblocks := len(inblocks)
	if num_inblocks != f.K {
		return nil, nil, fmt.Errorf("Precondition violation: Wrong length -- the slice of input blocks is required to contain exactly k blocks.  len(blocks): %d, k: %d\n", num_inblocks, f.K)
	}

	size := -1
	for i := range inblocks {
		n := len(inblocks[i])
		if size == -1 {
			size = n
		} else if size != n {
			return nil, nil, fmt.Errorf("Precondition violation: Input blocks are required to be all the same length.  Length of one block was: %d, length of another block was: %d", size, n)
		}
	}

	inputblocks := make([]*byte, num_inblocks)

	inblockbuffer := make([]byte, size*num_inblocks)
	for i := 0; i < num_inblocks; i++ {
		copy(inblockbuffer[i*size:], inblocks[i])
		inputblocks[i] = &inblockbuffer[i*size]
	}

	// num of secondary blocks = m - k
	n := f.M - f.K
	outblocks := make([]*byte, n)
	outblocksbuffer := make([]byte, size*n)

	for i := 0; i < n; i++ {
		outblocks[i] = &outblocksbuffer[i*size]
	}

	block_nums := make([]C.uint, n)
	for i := 0; i < n; i++ {
		block_nums[i] = C.uint(f.K + i)
	}

	// We need this if goroutine invokes this metehod, otherwise segmentation fault sometimes
	runtime.LockOSThread()
	C.fec_encode(
		(*C.fec_t)(f.Ptr),
		(**C.gf)(unsafe.Pointer(&inputblocks[0])),
		(**C.gf)(unsafe.Pointer(&outblocks[0])),
		(*C.uint)(unsafe.Pointer(&block_nums[0])),
		C.size_t(n), C.size_t(size))

	outblocknums := make([]int, n)
	outbuffers := make([][]byte, n)
	for i := 0; i < n; i++ {
		outbuffers[i] = outblocksbuffer[i*size : (i*size)+size]
		outblocknums[i] = int(f.K) + i
	}
	return outbuffers, outblocknums, nil
}

// Decode blocks.  There should be k number of blocks.
// Input blocks are passed as an array of references to Buffers.
// An int array contains each block's corresponding share number.
// The padding length is also required.
// Returns decoded data as a byte slice.
func (f Zfec) Decode(blocks [][]byte, blocknums []int, padlen int) ([]byte, error) {
	numblocks := len(blocks)
	if numblocks != f.K {
		return nil, fmt.Errorf("Precondition violation: Wrong length -- number of input blocks should contain exactly k blocks.  len(input): %d, k: %d", numblocks, f.K)
	}

	if len(blocknums) != f.K {
		return nil, fmt.Errorf("Precondition violation: Wrong length -- blocknums is required to contain exactly k blocks.  len(blocknums): %d, k: %d", len(blocknums), f.K)
	}

	size := -1
	for i := range blocks {
		n := len(blocks[i])
		if size == -1 {
			size = n
		} else if size != n {
			return nil, fmt.Errorf("Precondition violation: Input blocks are required to be all the same length.  Length of one block was: %d, length of another block was: %d", size, n)
		}
	}

	numprimary := 0
	m := make(map[int][]byte)

	indexes := make([]C.uint, numblocks)
	inblocks := make([]*byte, numblocks)
	// Populate with primary blocks in the same index position as their share number
	for i := 0; i < numblocks; i++ {
		b := blocks[i]
		share := blocknums[i]
		if share < f.K {
			numprimary++
			inblocks[share] = &b[0]
			m[share] = blocks[i]
			indexes[share] = C.uint(share)
		}
	}
	// Fill up empty positions with secondary blocks
	j := 0
	for i := 0; i < numblocks; i++ {
		b := blocks[i]
		share := blocknums[i]

		if share >= f.K {
			// find first empty inblock
			for inblocks[j] != nil {
				j++
			}
			inblocks[j] = &b[0]
			indexes[j] = C.uint(share)
		}
	}

	//	fmt.Printf("indexes = %v\n", indexes)

	//	numoutblocks := numblocks - numprimary
	numoutblocks := numblocks
	outblocksbuffer := make([]byte, numoutblocks*size)
	outblocks := make([]*byte, numoutblocks)
	//fmt.Printf("numoutblocks = %d\n", numoutblocks)
	for i := 0; i < numoutblocks; i++ {
		outblocks[i] = &outblocksbuffer[i*size]
	}

	C.fec_decode(
		(*C.fec_t)(f.Ptr),
		(**C.gf)(unsafe.Pointer(&inblocks[0])),
		(**C.gf)(unsafe.Pointer(&outblocks[0])),
		(*C.uint)(unsafe.Pointer(&indexes[0])),
		C.size_t(size))

	// Construct the decoded buffer
	var result []byte
	j = 0
	for i := 0; i < f.K; i++ {
		var b []byte
		if m[i] != nil {
			b = m[i]

		} else {
			p := j * size
			b = outblocksbuffer[p : p+size]
			j++
		}
		if i+1 == f.K {
			c := len(b) - padlen
			b = b[:c]
		}
		result = append(result, b...)
	}

	return result, nil
}
