The zfec package provides a Go binding for the zfec fast forward error correction library, with the bindings similar to the Python API.

Prerequisites:

The zfec package imports the gnuflag package, so you need to import that package which is a bazaar repo.
e.g.
python easy_install bzr
go get launchpad.net/gnuflag

You will also need a C compiler as the package includes the original zfec library C files.  On Mac OS X you should use at least Go 1.1.2 RC3 which resolves many cgo problems with XCode 5 and Clang.

To install zfec run:

go get gitlab.com/zfec/go-zfec

To use zfec in your programs:

import "gitlab.com/zfec/go-zfec"

To read documentation:

http://godoc.org/gitlab.com/zfec/go-zfec

More information about the zfec library can be found here: https://pypi.python.org/pypi/zfec

Benchmarks:

EncodeReaderParallel() lets you take advantage of multiple cores. From running some test benchmarks using the Python and Go versions of the zfec command line tool, we find that on a multi-core processor the Go zfec can run twice as fast as the Python zfec.

Linux 3.8.0-32-generic (x86_64), Core i7-2640M
- Python 2.7.4
- Go version devel +f4d1cb8d9a91
- Data file is xubuntu-13.10-desktop-amd64.iso (882,900,992 bytes)

Encoding time with k=3, m=8 (times are average of 3 runs, lower is better)
    Python = 7.49733 secs
 Go 1 core = 5.51867
Go 2 cores = 4.92233
Go 4 cores = 3.635

Encoding time with k=10, m=30
    Python = 15.05467 secs
 Go 1 core = 14.96633
Go 2 cores = 9.86467 
Go 4 cores = 8.26233

The results on Mac OS X do not show the same improvement as the number of cores increases.  No detailed profiling has been undertaken yet to determine the reason for this.

Mac OS X 10.9, Core i7-3615QM
- Python 2.7.5
- Go version 1.2 RC 3
- Data file is xcode_5_gm_seed.dmg (2,046,806,401 bytes)

Encoding time with k=3, m=8 (times are average of 3 runs, lower is better)
    Python = 28.73 secs
 Go 1 core = 21.334
Go 4 cores = 20.79267
Go 8 cores = 21.19067

Encoding time with k=10, m=30
    Python = 51.273 secs
 Go 1 core = 36.63267
Go 4 cores = 24.118
Go 8 cores = 24.90633

