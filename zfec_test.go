package zfec

/* zfec_test.go
 *
 * Copyright (C) 2013 Simon Liu 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

import (
	"bytes"
	"crypto/rand"
	"crypto/sha512"
	"fmt"
	"io"
	"io/ioutil"
	mrand "math/rand"
	"os"
	"strings"
	"testing"
	"time"
)

func Test_bad_args_construct_encoder_decoder(t *testing.T) {
	_, err := Init(-1, -1)
	if err == nil {
		t.Error("Should have gotten an exception from out-of-range arguments.")
	}
	_, err = Init(1, 257)
	if err == nil {
		t.Error("Should have gotten an exception from out-of-range arguments.")
	}
	_, err = Init(3, 2)
	if err == nil {
		t.Error("Should have gotten an exception from out-of-range arguments.")
	}
}

// _help_test_filefec is a helper function to encode a string and verify decoding.
// Share files are created in a temporary folder which is removed on return.
func _help_test_filefec(msg string, k int, m int, numshs int) (err error) {
	if numshs == 0 {
		numshs = m
	}

	testfilename := "testfile.txt"
	prefix := "test_"
	suffix := "fec"

	tempdir, err := ioutil.TempDir("", "zfec_test")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tempdir)
	//fmt.Printf("tempdir = %s\n", tempdir)

	f, err := ioutil.TempFile(tempdir, testfilename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.WriteString(f, msg)
	if err != nil {
		return err
	}

	fec, err := Init(k, m)
	if err != nil {
		return err
	}
	defer fec.Dealloc()
	sharefiles, err := fec.EncodeToFiles(f.Name(), tempdir, testfilename, prefix, suffix, true, false, 0)

	if err != nil {
		return err
	}

	// shuffle the share files and take k of them to use for decoding
	r := mrand.New(mrand.NewSource(time.Now().UnixNano()))
	indices := r.Perm(m)
	shuffledShareFiles := make([]string, k)
	for i := 0; i < k; i++ {
		index := indices[i]
		shuffledShareFiles[i] = sharefiles[index]
	}

	df, err := ioutil.TempFile(tempdir, "file")
	if err != nil {
		return err
	}
	defer df.Close()

	err = DecodeToFile(df.Name(), shuffledShareFiles, true, false)
	if err != nil {
		return err
	}

	// Verify
	b, err := ioutil.ReadFile(df.Name())
	if err != nil {
		return err
	}
	s := string(b)
	if !strings.EqualFold(s, msg) {
		return fmt.Errorf("Parameter '%s' not equal to decoded '%s'", msg, s)
	}

	return nil
}

func Test_filefec(t *testing.T) {
	err := _help_test_filefec("Yellow Whirled!", 3, 8, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!", 4, 16, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled", 3, 8, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled", 4, 16, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirle", 3, 8, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirle", 4, 16, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!A", 3, 8, 0)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!A", 3, 8, 3)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("llow Whirled!A\r\n", 3, 8, 3)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!A\n", 3, 8, 3)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!A\n\n", 3, 8, 3)
	if err != nil {
		t.Error(err.Error())
	}
	err = _help_test_filefec("Yellow Whirled!A\r\r\n\n", 3, 8, 3)
	if err != nil {
		t.Error(err.Error())
	}
}

func Test_Cmdline_test_basic(t *testing.T) {
	err := _help_test_filefec("WHEHWHJEKWAHDLJAWDHWALKDHA", 3, 8, 0)
	if err != nil {
		t.Error(err.Error())
	}
}

func Test_FileFecHeader(t *testing.T) {
	m_array := []int{1, 2, 3, 5, 7, 9, 11, 17, 19, 33, 35, 65, 66, 67, 129, 130, 131, 254, 255, 256}
	k_array := []int{1, 2, 3, 5, 9, 17, 33, 65, 129, 255, 256}
	for _, m := range m_array {
		for _, k := range k_array {
			if k >= m {
				continue
			}
			pad_array := []int{0, 1, k - 1}
			for _, pad := range pad_array {
				if pad >= k {
					continue
				}
				sh_array := []int{0, 1, m - 1}
				for _, sh := range sh_array {
					if sh >= m {
						continue
					}
					header, err := BuildZfecFileHeaderForShare(m, k, sh, pad)
					//fmt.Printf("m=%d, k=%d, pad=%d, sh=%d, headerlen=%d\n", m, k, pad, sh, len(header))
					if err != nil {
						t.Error(err.Error())
					}
					len2, m2, k2, pad2, sh2, err2 := ParseZfecFileHeader(header)
					if err2 != nil || len2 != len(header) || m != m2 || k != k2 || pad != pad2 || sh != sh2 {
						t.Errorf("Error parsing file header: m=%d, k=%d, pad=%d, sh=%d, headerlen=%d\n", m2, k2, pad2, sh2, len2)
					}
				}
			}
		}
	}

}

// This test checks to make sure that if the encoded fec share data (less the header), is of a length
// n times the chunk size (which is normally 4096), the padding is removed.
// "bug with m=16 k=13 parameters at certain file length"
// https://tahoe-lafs.org/pipermail/tahoe-dev/2013-September/008702.html
func Test_ShareDataIsMultipleOfChunkSize(t *testing.T) {
	dir, err := ioutil.TempDir("", "zfec_test")
	if err != nil {
		t.Error(err.Error())
	}
	defer os.RemoveAll(dir)

	f, err := ioutil.TempFile(dir, "file")
	if err != nil {
		t.Error(err.Error())
	}
	defer f.Close()

	// create random file of length which caused a bug as discussed in zfec mailing list
	c := 6176761
	b := make([]byte, c)
	n, err := io.ReadFull(rand.Reader, b)
	if n != len(b) || err != nil {
		t.Error(err.Error())
	}

	buf := bytes.NewBuffer(b)

	h := sha512.New()
	_, err = h.Write(b)

	if err != nil {
		t.Error(err.Error())
	}
	digest := h.Sum(nil)
	//fmt.Printf("sha512 = %x\n", digest)

	_, err = buf.WriteTo(f)

	fec, err := Init(13, 16)
	if err != nil {
		t.Error(err.Error())
	}
	defer fec.Dealloc()

	sharefiles, err := fec.EncodeToFiles(f.Name(), dir, "base", "", "fec", true, false, 0)
	if err != nil {
		t.Fatal(err.Error())
	}

	df, err := ioutil.TempFile(dir, "file")
	if err != nil {
		t.Error(err.Error())
	}
	defer df.Close()

	err = DecodeToFile(df.Name(), sharefiles, true, false)
	if err != nil {
		t.Error(err.Error())
	}

	db, _ := ioutil.ReadAll(df)
	h.Reset()
	_, err = h.Write(db)
	if err != nil {
		t.Error(err.Error())
	}
	digest2 := h.Sum(nil)
	//fmt.Printf("sha512 = %x\n", digest)

	if !bytes.Equal(digest, digest2) {
		t.Error("SHA512 digests of original and decoded data are different")
	}
}
